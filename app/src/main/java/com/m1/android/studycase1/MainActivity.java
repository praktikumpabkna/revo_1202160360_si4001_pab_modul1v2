package com.m1.android.studycase1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private EditText Ealas;
    private EditText Etinggi;
    private TextView Hhasile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    protected void hitung(View view){
        Ealas = findViewById(R.id.editText1);
        Etinggi = findViewById(R.id.editText2);
        Hhasile = findViewById(R.id.editText4);

        Integer alas = Integer.parseInt(Ealas.getText().toString());
        Integer tinggi = Integer.parseInt(Etinggi.getText().toString());
        Integer hasile = alas*tinggi;
        Hhasile.setText(String.valueOf(hasile));
}}
